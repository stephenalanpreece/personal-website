function loadContact() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("bg").innerHTML =
                this.responseText;
        }
    };
    xhttp.open("GET", "contact.html", true);
    xhttp.send();
}

function loadCV() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("bg").innerHTML =
                this.responseText;
        }
    };
    xhttp.open("GET", "cv.html", true);
    xhttp.send();
}

function loadHome() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("bg").innerHTML =
                this.responseText;
        }
    };
    xhttp.open("GET", "index.html", true);
    xhttp.send();
}

function loadProjects() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("bg").innerHTML =
                this.responseText;
        }
    };
    xhttp.open("GET", "projects.html", true);
    xhttp.send();
}